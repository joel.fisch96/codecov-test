import { appReducer, decrement, increment, reset } from './reducer';

describe('appReducer', () => {
  test('increment()', () =>
    expect(appReducer({ counter: 4 }, increment())).toEqual({
      counter: 5,
    }));

  test('increment() with negatives', () =>
    expect(appReducer({ counter: -3 }, increment())).toEqual({
      counter: -2,
    }));

  test('decrement()', () =>
    expect(appReducer({ counter: -3 }, decrement())).toEqual({
      counter: -4,
    }));

  test('decrement() with negatives', () =>
    expect(appReducer({ counter: 9 }, decrement())).toEqual({
      counter: 8,
    }));

  test('reset()', () =>
    expect(appReducer({ counter: 9 }, reset())).toEqual({
      counter: 0,
    }));

  test('reset() with negatives', () =>
    expect(appReducer({ counter: -9 }, reset())).toEqual({
      counter: 0,
    }));
});

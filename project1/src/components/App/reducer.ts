import { createActionCreator, createReducer } from 'deox';

export const increment = createActionCreator('INCREMENT');
export const decrement = createActionCreator('DECREMENT');
export const reset = createActionCreator('RESET');

export type AppState = {
  readonly counter: number;
};

const initialState: AppState = {
  counter: 0,
};

export const appReducer = createReducer(initialState, handleAction => [
  handleAction(increment, state => ({
    ...state,
    counter: state.counter + 1,
  })),
  handleAction(decrement, state => ({
    ...state,
    counter: state.counter - 1,
  })),
  handleAction(reset, state => ({
    ...state,
    counter: 0,
  })),
]);

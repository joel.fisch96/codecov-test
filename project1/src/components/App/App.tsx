import React from 'react';
import { appReducer, decrement, increment, reset } from './reducer';

const App: React.FC = () => {
  const [state, dispatch] = React.useReducer(
    appReducer,
    appReducer(undefined, {} as any)
  );
  return (
    <div>
      <h1>Hello Project 1</h1>
      <p>Counter: {state.counter}</p>
      <button onClick={() => dispatch(increment())}>+</button>
      <button onClick={() => dispatch(decrement())}>-</button>
      <button onClick={() => dispatch(reset())}>Reset</button>
    </div>
  );
};

export default App;

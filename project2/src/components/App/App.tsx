import React from 'react';
import { appReducer, setName, toggle } from './reducer';

const App: React.FC = () => {
  const [state, dispatch] = React.useReducer(
    appReducer,
    appReducer(undefined, {} as any)
  );
  return (
    <div>
      <h1>Hello Project 2</h1>
      <button onClick={() => dispatch(toggle())}>Toggle</button>
      <input
        type="text"
        onChange={evt => dispatch(setName(evt.target.value))}
      />
      {state.isVisible && <p>{state.name}</p>}
    </div>
  );
};

export default App;

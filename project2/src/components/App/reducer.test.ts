import { appReducer, toggle } from './reducer';

describe('appReducer', () => {
  test('toggle with visible', () =>
    expect(appReducer({ isVisible: true, name: '' }, toggle())).toEqual({
      isVisible: false,
      name: '',
    }));

  test('toggle with not visible', () =>
    expect(appReducer({ isVisible: false, name: '' }, toggle())).toEqual({
      isVisible: true,
      name: '',
    }));
});

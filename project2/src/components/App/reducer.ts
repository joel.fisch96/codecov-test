import { createActionCreator, createReducer } from 'deox';

export const setName = createActionCreator(
  'SET_NAME',
  resolve => (name: string) => resolve({ name })
);

export const toggle = createActionCreator('TOGGLE');

export type AppState = {
  readonly isVisible: boolean;
  readonly name: string;
};

const initialState: AppState = {
  isVisible: false,
  name: '',
};

export const appReducer = createReducer(initialState, handleAction => [
  handleAction(toggle, state => ({
    ...state,
    isVisible: !state.isVisible,
  })),
  handleAction(setName, (state, { payload }) => ({
    ...state,
    name: payload.name,
  })),
]);
